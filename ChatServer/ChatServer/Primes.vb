﻿Public Class Primes
    Public Shared Function GenerateRandomPrimRoot(ByVal prime As Integer) As Integer
        Dim o As Integer = 1, k As Integer = 0, z As Integer = 0
        Dim table As Hashtable = New Hashtable
        Dim r As Integer = 2
        While r < prime
            k = Math.Pow(r, o)
            k = k Mod prime
            While k > 1
                o = o + 1
                k = k * r
                k = k Mod prime
            End While
            If o = (prime - 1) Then
                table(z) = r
                z = z + 1
            End If
            o = 1
            r = r + 1
        End While
        Dim rnd As New Random
        Return (table(rnd.Next(1, z)))
    End Function

    Public Shared Function GenerateRandomPrime(ByVal max As Integer) As Integer
        Dim bol As Boolean = False
        Dim rnd As New Random
        Dim temp As Integer
        While bol = False
            temp = rnd.Next(1, max)
            If Fast(temp) = True Then
                bol = True
            End If
        End While
        Return temp
    End Function

    Public Shared Function Fast(ByVal n As Integer) As Boolean
        If n Mod 2 = 0 OrElse n Mod 3 = 0 OrElse n Mod 7 = 0 OrElse n Mod 5 = 0 Then Return False
        Dim r As Single = Math.Sqrt(n), f As Short = 11
        If r Mod 1 = 0 Then Return False
        While f <= r
            If n Mod f = 0 Then Return False
            f += 2
            If n Mod f = 0 Then Return False
            f += 4
        End While
        Return True
    End Function

    Public Shared Function random_key(ByVal lenght As Integer) As String
        Randomize()
        Dim s As New System.Text.StringBuilder("")
        Dim b() As Char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".ToCharArray()
        For i As Integer = 1 To lenght
            Randomize()
            Dim z As Integer = Int(((b.Length - 2) - 0 + 1) * Rnd()) + 1
            s.Append(b(z))
        Next
        Return s.ToString
    End Function
End Class
