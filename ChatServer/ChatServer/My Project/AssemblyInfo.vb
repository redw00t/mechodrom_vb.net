﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Allgemeine Informationen über eine Assembly werden über die folgenden 
' Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
' die mit einer Assembly verknüpft sind.

' Die Werte der Assemblyattribute überprüfen

<Assembly: AssemblyTitle("CryptoChatServer")> 
<Assembly: AssemblyDescription("Server for the CryptoChat Protocol")> 
<Assembly: AssemblyCompany("redw00t")> 
<Assembly: AssemblyProduct("CryptoChatClient")> 
<Assembly: AssemblyCopyright("Copyright redw00t ©  2013")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
<Assembly: Guid("a8536fd2-f03a-427f-b2a9-f444702a6ae1")> 

' Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
'
'      Hauptversion
'      Nebenversion 
'      Buildnummer
'      Revision
'
' Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
' übernehmen, indem Sie "*" eingeben:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.0.2.4")> 
<Assembly: AssemblyFileVersion("0.0.2.4")> 

<Assembly: NeutralResourcesLanguageAttribute("en")> 