﻿'0010100001100011001010010010000001100010011110010010000001110010011001010110010001110111001100000011000001110100

Imports System.Net.Sockets
Imports System.IO
Imports System.Net
Imports System.Text.RegularExpressions
Imports System.Threading

Module Module1
    Private server As TcpListener
    Private client As New TcpClient
    Private ipendpoint As IPEndPoint = New IPEndPoint(IPAddress.Any, 1551)
    Private list As New List(Of Connection)
    Dim rnd As New Random

    Private Structure Connection
        Dim stream As NetworkStream
        Dim streamw As StreamWriter
        Dim streamr As StreamReader
        Dim nick As String
        Dim channel As String
        Dim AES_Key As String
        Dim RSA_PublicKey As String
        Dim RSA_PrivateKey As String
    End Structure

    Sub Main()
        Console.Title = "CryptoChatServer" : Console.WriteLine("Server is running!")
        server = New TcpListener(ipendpoint) : server.Start()

        'Infite Loop accepts Clients.
        While True
            client = server.AcceptTcpClient
            'Defines Client 
            Dim c As New Connection
            c.stream = client.GetStream
            c.streamr = New StreamReader(c.stream)
            c.streamw = New StreamWriter(c.stream)
            Dim RSA_key As Cryption.RSA_key_Struct = Cryption.Create_RSA_Key

            'RSA Public Key Exchange
            c.RSA_PublicKey = c.streamr.ReadLine
            c.streamw.WriteLine(RSA_key.open_key) : c.streamw.Flush()

            'Writes Private Key to Structure.
            c.RSA_PrivateKey = RSA_key.privat_key

            'Diffi Hellman
            Dim p As Integer
            Dim g As Integer
            Dim tmp As String = c.streamr.ReadLine
            If CheckCerf(tmp, c) = True Then
                p = Integer.Parse(Cryption.RSA_ent(GetHandshakeMessage(tmp), RSA_key.privat_key))
            Else
                c.stream.Close()
            End If

            tmp = c.streamr.ReadLine
            If CheckCerf(tmp, c) = True Then
                g = Integer.Parse(Cryption.RSA_ent(GetHandshakeMessage(tmp), RSA_key.privat_key))
            Else
                c.stream.Close()
            End If

            Dim b As Integer = Rnd.Next(1, p - 2)
            Dim _B As Integer = Modular_Pow(g, b, p)

            tmp = c.streamr.ReadLine
            If CheckCerf(tmp, c) = True Then
                c.AES_Key = Cryption.MD5StringHash(Modular_Pow(Integer.Parse(Cryption.RSA_ent(GetHandshakeMessage(tmp), RSA_key.privat_key)), b, p))
            Else
                c.stream.Close()
            End If
            SendHandshakeString(Cryption.RSA_ver(_B.ToString, c.RSA_PublicKey), c)

            'Handshake
            tmp = c.streamr.ReadLine
            If CheckCerf(tmp, c) = True Then
                c.nick = GetMessage(tmp, c)
            Else
                c.streamr.Close()
            End If

            tmp = c.streamr.ReadLine
            If CheckCerf(tmp, c) = True Then
                c.channel = GetMessage(tmp, c)
            Else
                c.streamr.Close()
            End If

            'Display's Connection in Console.
            Console.WriteLine("{0} has joined. Key: {1}. Channel: {2}. Prime: {3}. Primitiv Root: {4}", c.nick, c.AES_Key, c.channel, p, g)

            'Add's Connection to list(Of String)
            list.Add(c)

            SendNickToAllClients()

            'Start Listening Thread foreach Connection.
            Dim t As New Threading.Thread(AddressOf ListenToConnection) : t.Start(c)
        End While
    End Sub

    ''' <summary>
    ''' Sends String to every Client in Channel, expect the Connection ByVal con.
    ''' </summary>
    ''' <param name="s">String</param>
    ''' <param name="con">Connection</param>
    ''' <remarks></remarks>
    Private Sub SendToAllClientsExMe(ByVal s As String, ByVal con As Connection)
        For Each c As Connection In list
            If Not con.nick = c.nick AndAlso con.channel = c.channel Then
                Try
                    SendString(s, c)
                Catch
                End Try
            End If
        Next
    End Sub

    ''' <summary>
    ''' Sends String to really every Connection.
    ''' </summary>
    ''' <param name="s">String</param>
    ''' <remarks></remarks>
    Private Sub SendToAllClients(ByVal s As String)
        For Each c As Connection In list
            Try
                SendString(s, c)
            Catch
            End Try
        Next
    End Sub

    ''' <summary>
    ''' Handles Userlist, get's Nick from every Connection in List and sends it.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SendNickToAllClients()
        Dim rnd As New Random()
        For Each c As Connection In list
            Dim n As String = ""
            For Each con As Connection In list
                If c.channel = con.channel Then
                    n += con.nick & {Chr(59)}
                End If
                Try
                    Dim te As String = Cryption.Rijndaelcrypt("!USERLIST " & n, c.AES_Key)
                    c.streamw.WriteLine(te & " ~ " & Cryption.RSA_ver(Cryption.MD5StringHash(te), c.RSA_PublicKey))
                    c.streamw.Flush()
                Catch ex As Exception
                    list.Remove(c)
                    Console.WriteLine("{0} has exit.", c.nick)
                End Try
            Next
        Next
        Thread.Sleep(2000)
    End Sub

    ''' <summary>
    ''' Thread created for every Connection.
    ''' </summary>
    ''' <param name="con"></param>
    ''' <remarks></remarks>
    Private Sub ListenToConnection(ByVal con As Connection)
        Do
            Try
                'Get's Message
                Dim tmp As String = con.streamr.ReadLine
                'Check's if Message is certified.
                If CheckCerf(tmp, con) = False Then
                    con.streamw.WriteLine("&4Hijacked please leave now!") : con.streamw.Flush()
                End If

                'Encrypts Message
                tmp = GetMessage(tmp, con)

                'Identifys Message
                If tmp = "!EXIT" Then
                    If list.Contains(con) Then
                        list.Remove(con)
                        Console.WriteLine("{0} has exit.", con.nick)
                        SendNickToAllClients()
                        SendToAllClients(rnd.Next(100, 999) & "&9" & con.nick & " has exit." & rnd.Next(100, 999))
                    End If
                    '!MSG
                ElseIf tmp.StartsWith("!MSG ") Then
                    Dim m As Match = Regex.Match(tmp.Replace("!MSG ", ""), "^([^\s]+) (.*)")
                    For Each c As Connection In list
                        If c.nick = m.Groups(1).ToString Then
                            Console.WriteLine("PM: from " & con.nick & "--> " & c.nick)
                            SendString(rnd.Next(100, 999) & "&5Private Message from: " & con.nick & " -> " & m.Groups(2).ToString & rnd.Next(100, 999), c)
                        End If
                    Next
                    '!POKE
                ElseIf tmp.StartsWith("!POKE ") Then
                    Dim m As Match = Regex.Match(tmp, "!POKE (.*) (.*) (.*)")
                    For Each c As Connection In list
                        If c.nick = m.Groups(1).ToString Then
                            SendString("!POKE " & m.Groups(1).ToString & " " & m.Groups(2).ToString & " " & m.Groups(3).ToString, c)
                        End If
                    Next
                    '!FILE
                ElseIf tmp.StartsWith("!FILE ") Then
                    Dim m As Match = Regex.Match(tmp, "!FILE (.*) (.*) (.*)")
                    For Each c As Connection In list
                        If c.nick = m.Groups(1).ToString Then
                            SendString("!FILE " & con.nick & " " & m.Groups(2).ToString & " " & m.Groups(3).ToString, c)
                            Console.WriteLine("!FILE " & con.nick & " " & m.Groups(2).ToString & " " & m.Groups(3).ToString)
                        End If
                    Next
                    'Normal Message
                Else
                    If Not tmp = "" Then
                        Console.WriteLine(con.nick & ": """ & tmp & """")
                    End If
                    SendToAllClientsExMe(tmp, con)
                End If
                'Catches disconnect.
            Catch
                list.Remove(con)
                Console.WriteLine("{0} has exit.", con.nick)
                SendNickToAllClients()
                Exit Do
            End Try
        Loop
    End Sub

#Region "Functions/Subs"
    ''' <summary>
    ''' Modular Pow needed for Diffi Hellman.
    ''' </summary>
    ''' <param name="a"></param>
    ''' <param name="b"></param>
    ''' <param name="m"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Modular_Pow(ByVal a As Long, ByVal b As Long, ByVal m As Long) As Long
        Dim result As Long = 1
        While b > 0
            If (b And 1) = 1 Then
                result = (result * a) Mod m
            End If
            b >>= 1
            a = (a * a) Mod m
        End While
        Return result
    End Function

    ''' <summary>
    ''' Sends String.
    ''' Encrypts it -> Certificates it.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <remarks></remarks>
    Private Function CheckCerf(ByVal msg As String, ByVal con As Connection) As Boolean
        Dim arry As String() = msg.Split(" ~ ")
        Dim tmp_hash As String = Cryption.RSA_ent(arry(2), con.RSA_PrivateKey)
        If Not Cryption.MD5StringHash(arry(0)) = tmp_hash Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sends String.
    ''' Certificates it.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <remarks></remarks>
    Private Sub SendHandshakeString(ByVal msg As String, ByVal con As Connection)
        con.streamw.WriteLine(msg & " ~ " & Cryption.RSA_ver(Cryption.MD5StringHash(msg), con.RSA_PublicKey)) : con.streamw.Flush()
    End Sub

    ''' <summary>
    ''' Check String, if it's certificated.
    ''' </summary>
    ''' <param name="msg">Received Message</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Sub SendString(ByVal msg As String, ByVal con As Connection)
        msg = Cryption.Rijndaelcrypt(msg, con.AES_Key)
        con.streamw.WriteLine(msg & " ~ " & Cryption.RSA_ver(Cryption.MD5StringHash(msg), con.RSA_PublicKey)) : con.streamw.Flush()
    End Sub

    ''' <summary>
    ''' Extracts the Message without Certificat.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetMessage(ByVal msg As String, ByVal con As Connection) As String
        Dim arry As String() = msg.Split(" ~ ")
        Return Cryption.RijndaelDecrypt(arry(0), con.AES_Key)
    End Function

    ''' <summary>
    ''' Extracts the Message without Certificat.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetHandshakeMessage(ByVal msg As String) As String
        Dim arry As String() = msg.Split(" ~ ")
        Return arry(0)
    End Function
#End Region
End Module