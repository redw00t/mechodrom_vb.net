﻿Imports System.Security.Cryptography
Imports System.Text

Public Class Cryption

    Structure RSA_key_Struct
        Dim open_key As String
        Dim privat_key As String
    End Structure

    Public Shared Function Create_RSA_Key(Optional ByVal keysize As Integer = 2048) As RSA_key_Struct
        Dim RSA_key As RSA_key_Struct
        Dim RSA_Crypto As New RSACryptoServiceProvider(keysize)
        RSA_key.open_key = RSA_Crypto.ToXmlString(False)
        RSA_key.privat_key = RSA_Crypto.ToXmlString(True)
        Return RSA_key
    End Function

    Public Shared Function RSA_ver(ByVal DATA As String, ByVal Openkey_Xml As String, Optional ByVal keysize As Integer = 2048) As String
        Dim RSA_Crypto_ver As New RSACryptoServiceProvider(keysize)
        Dim byte_data As Byte() = UTF8Encoding.UTF8.GetBytes(DATA)
        Dim Out_String As String = String.Empty
        RSA_Crypto_ver.FromXmlString(Openkey_Xml)
        Out_String = Convert.ToBase64String(RSA_Crypto_ver.Encrypt(byte_data, True))
        Return Out_String
    End Function

    Public Shared Function RSA_ent(ByVal DATA As String, ByVal Privatekey_Xml As String, Optional ByVal keysize As Integer = 2048) As String
        Dim RSA_Crypto_ent As New RSACryptoServiceProvider(keysize)
        Dim byte_data As Byte() = Convert.FromBase64String(DATA)
        Dim Out_String As String = String.Empty
        RSA_Crypto_ent.FromXmlString(Privatekey_Xml)
        Out_String = Encoding.Default.GetString(RSA_Crypto_ent.Decrypt(byte_data, True))
        Return Out_String
    End Function

    Public Shared Function Rijndaelcrypt(ByVal File As String, ByVal Key As String)
        Dim oAesProvider As New RijndaelManaged
        Dim btClear() As Byte
        Dim btSalt() As Byte = New Byte() {1, 2, 3, 4, 5, 6, 7, 8}
        Dim oKeyGenerator As New Rfc2898DeriveBytes(Key, btSalt)
        oAesProvider.Key = oKeyGenerator.GetBytes(oAesProvider.Key.Length)
        oAesProvider.IV = oKeyGenerator.GetBytes(oAesProvider.IV.Length)
        Dim ms As New IO.MemoryStream
        Dim cs As New CryptoStream(ms, _
          oAesProvider.CreateEncryptor(), _
          CryptoStreamMode.Write)
        btClear = System.Text.Encoding.UTF8.GetBytes(File)
        cs.Write(btClear, 0, btClear.Length)
        cs.Close()
        File = Convert.ToBase64String(ms.ToArray)
        Return File
    End Function

    Public Shared Function RijndaelDecrypt(ByVal UDecryptU As String, ByVal UKeyU As String)
        Dim XoAesProviderX As New RijndaelManaged
        Dim XbtCipherX() As Byte
        Dim XbtSaltX() As Byte = New Byte() {1, 2, 3, 4, 5, 6, 7, 8}
        Dim XoKeyGeneratorX As New Rfc2898DeriveBytes(UKeyU, XbtSaltX)
        XoAesProviderX.Key = XoKeyGeneratorX.GetBytes(XoAesProviderX.Key.Length)
        XoAesProviderX.IV = XoKeyGeneratorX.GetBytes(XoAesProviderX.IV.Length)
        Dim XmsX As New IO.MemoryStream
        Dim XcsX As New CryptoStream(XmsX, XoAesProviderX.CreateDecryptor(), _
          CryptoStreamMode.Write)
        Try
            XbtCipherX = Convert.FromBase64String(UDecryptU)
            XcsX.Write(XbtCipherX, 0, XbtCipherX.Length)
            XcsX.Close()
            UDecryptU = System.Text.Encoding.UTF8.GetString(XmsX.ToArray)
        Catch
        End Try
        Return UDecryptU
    End Function

    Public Shared Function MD5StringHash(ByVal strString As String) As String
        Dim MD5 As New MD5CryptoServiceProvider
        Dim Data As Byte()
        Dim Result As Byte()
        Dim Res As String = ""
        Dim Tmp As String = ""

        Data = Encoding.ASCII.GetBytes(strString)
        Result = MD5.ComputeHash(Data)
        For i As Integer = 0 To Result.Length - 1
            Tmp = Hex(Result(i))
            If Len(Tmp) = 1 Then Tmp = "0" & Tmp
            Res += Tmp
        Next
        Return Res
    End Function
End Class