﻿'0010100001100011001010010010000001100010011110010010000001110010011001010110010001110111001100000011000001110100

Imports System.Net.Sockets
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

    Dim nickname, ip, channel As String

    Private stream As NetworkStream
    Private streamw As StreamWriter
    Private streamr As StreamReader
    Private client As New TcpClient
    Private t As New Threading.Thread(AddressOf Listen)
    Private r As New Threading.Thread(AddressOf Refresh)

#Region "Diffi"
    Dim RSA_key As Cryption.RSA_key_Struct = Cryption.Create_RSA_Key
    Dim pub_key As String
    Dim rnd As New Random
    Dim p As Integer
    Dim g As Integer
    Dim a As Integer
    Dim _A As Integer
    Dim K As String
#End Region

#Region "Delegates"

    Dim Delegate_AppendText As AppendText = AddressOf InvokedProc_AppendText
    Dim Delegate_ClearItems As ClearItems = AddressOf InvokedProc_ClearItems
    Dim Delegate_AddRange As AddRange = AddressOf InvokedProc_AddRange
    Dim Delegate_Remove As Remove = AddressOf InvokedProc_Remove
    Dim Delegate_SelectionStart As SelectionStart = AddressOf InvokedProc_SelectionStart
    Dim Delegate_SelectionColor As SelectionColor = AddressOf InvokedProc_SelectionColor
    Dim Delegate_Mark As Dele_Mark = AddressOf InvokedProc_Mark
    Dim Delegate_ColorString As Dele_ColorString = AddressOf InvokedProc_ColorString
    Dim Delegate_AddItem As Dele_AddItem = AddressOf InvokedProc_AddItem
    Dim Delegate_SaveFile As Dele_SaveFile = AddressOf InvokedProc_SaveFile

    Private Delegate Sub AppendText(ByVal Text As String, ByVal t As RichTextBox)
    Private Sub InvokedProc_AppendText(ByVal Text As String, ByVal t As RichTextBox)
        t.AppendText(Text)
        t.SelectionStart = t.TextLength
        t.ScrollToCaret()
    End Sub

    Private Delegate Sub ClearItems(ByVal t As ListBox)
    Private Sub InvokedProc_ClearItems(ByVal t As ListBox)
        t.Items.Clear()
    End Sub

    Private Delegate Sub AddRange(ByVal Text() As String, ByVal t As ListBox)
    Private Sub InvokedProc_AddRange(ByVal Text() As String, ByVal t As ListBox)
        t.Items.AddRange(Text)
    End Sub

    Private Delegate Sub Remove(ByVal Text As String, ByVal t As ListBox)
    Private Sub InvokedProc_Remove(ByVal Text As String, ByVal t As ListBox)
        t.Items.Remove(Text)
    End Sub

    Private Delegate Sub SelectionStart(ByVal Text As Integer, ByVal t As RichTextBox)
    Private Sub InvokedProc_SelectionStart(ByVal Text As Integer, ByVal t As RichTextBox)
        t.SelectionStart = Text
    End Sub

    Private Delegate Sub SelectionColor(ByVal Text As Color, ByVal t As RichTextBox)
    Private Sub InvokedProc_SelectionColor(ByVal Text As Color, ByVal t As RichTextBox)
        t.SelectionColor = Text
    End Sub

    Private Delegate Sub Dele_Mark(ByVal rtb As RichTextBox, ByVal Text As String, ByVal t As Color)
    Private Sub InvokedProc_Mark(ByVal rtb As RichTextBox, ByVal Text As String, ByVal t As Color)
        rtb.SelectionStart = rtb.Text.Length
        Dim oldcolor As Color = rtb.SelectionColor
        rtb.SelectionColor = t
        rtb.AppendText(Text)
        rtb.SelectionColor = oldcolor
    End Sub

    Private Delegate Sub Dele_ColorString(ByVal rtb As RichTextBox, ByVal Text As String)
    Private Sub InvokedProc_ColorString(ByVal rtb As RichTextBox, ByVal Text As String)
        If (Text.Length < 1) Then Return
        Dim OriginalColor As Color = rtb.ForeColor
        Try
            Dim MessageArray As String() = Text.Split(Text.Substring(Text.IndexOf("&"), 2))

            For Each Word As String In MessageArray
                Dim col As Color
                Try

                    rtb.SelectionStart = rtb.Text.Length
                    Dim oldcolor As Color = rtb.SelectionColor
                    Dim ColorCode As String = Word.Substring(0, 1)
                    Word = Word.Substring(1)
                    Select Case ColorCode
                        Case "0"
                            col = Color.Black
                        Case "1"
                            col = Color.FromArgb(100, 33, 38, 64) ' DarkBlue
                        Case "2"
                            col = Color.FromArgb(100, 70, 89, 75) ' DarkGreen
                        Case "3"
                            col = Color.FromArgb(100, 3, 81, 89) ' DarkCyan
                        Case "4"
                            col = Color.FromArgb(100, 140, 35, 44) ' DarkRed
                        Case "5"
                            col = Color.FromArgb(100, 165, 0, 82) ' DarkMagenta
                        Case "6"
                            col = Color.FromArgb(100, 255, 184, 0) ' Yellow
                        Case "7"
                            col = Color.FromArgb(100, 140, 140, 135) ' Gray
                        Case "8"
                            col = Color.FromArgb(100, 29, 33, 38) ' DarkGray
                        Case "9"
                            col = Color.FromArgb(100, 96, 191, 191) ' LightBlue
                        Case "a"
                            col = Color.FromArgb(100, 3, 166, 90) ' Green
                        Case "b"
                            col = Color.FromArgb(100, 4, 217, 217) ' Cyan
                        Case "c"
                            col = Color.FromArgb(100, 217, 35, 50) ' Red
                        Case "d"
                            col = Color.FromArgb(100, 204, 0, 102) ' Magenta
                        Case "e"
                            col = Color.FromArgb(100, 255, 210, 73) ' LightYellow
                        Case "f"
                            col = Color.White
                        Case "B"
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Bold)
                            rtb.AppendText(Word)
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Regular)
                            GoTo skip
                        Case "I"
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Italic)
                            rtb.AppendText(Word)
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Regular)
                            GoTo skip
                        Case "S"
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Strikeout)
                            rtb.AppendText(Word)
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Regular)
                            GoTo skip
                        Case "U"
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Underline)
                            rtb.AppendText(Word)
                            rtb.SelectionFont = New Font("Verdana", 9, FontStyle.Regular)
                            GoTo skip
                    End Select

                    rtb.SelectionColor = col
                    rtb.AppendText(Word)
                    rtb.SelectionColor = oldcolor
skip:
                Catch
                End Try
            Next
        Catch
        End Try
    End Sub

    Private Delegate Sub Dele_AddItem(ByVal Text As String, ByVal Text2 As String, ByVal t As ListView)
    Private Sub InvokedProc_AddItem(ByVal Text As String, ByVal Text2 As String, ByVal t As ListView)
        Dim lw As New ListViewItem
        lw.Text = Text
        lw.SubItems.Add(Text2)
        t.Items.Add(lw)
    End Sub

    Private Delegate Sub Dele_SaveFile(ByVal Text As String, ByVal Name As String)
    Private Sub InvokedProc_SaveFile(ByVal Text As String, ByVal Name As String)
        Dim sfd As New SaveFileDialog
        sfd.FileName = Name
        sfd.ShowDialog()
        System.IO.File.WriteAllBytes(sfd.FileName, Convert.FromBase64String(Text))
    End Sub
#End Region

    ''' <summary>
    ''' Handles frmMain Load.
    ''' Redraws TabControl1, download's 'Server List'.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Redraw TabControl1
        Me.TabControl1.Region = New Region(New RectangleF(Me.TabPage1.Left, Me.TabPage1.Top, Me.TabPage1.Width, Me.TabPage1.Height))
        r.Start()
    End Sub

    ''' <summary>
    ''' Handles Connect Button Click.
    ''' Changes Statuslabel Text, generates Prime and Primroot for Diffi Hellman.
    ''' Tries to connect to Server.
    ''' RSA Key Exchange -> Diffi Hellman -> Handshake -> Join Message.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_Connect_Click(sender As Object, e As EventArgs) Handles btn_Connect.Click
        'Changes Statuslabel Text
        nickname = txt_nickname.Text
        ip = txt_ip.Text
        channel = cb_channel.Text

        'Prepares Diffi-Hellman
        p = Primes.GenerateRandomPrime(ud_prime.Value)
        g = Primes.GenerateRandomPrimRoot(p)
        a = rnd.Next(1, p - 2)
        _A = Modular_Pow(g, a, p)

        'Try to connect to Server.
        Try
            client.Connect(ip, 1551)
        Catch ex As Exception
            MessageBox.Show("Could not connect to Server!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Application.Exit()
        End Try

        'Defines stream, streamw & streamr
        If client.Connected Then
            stream = client.GetStream
            streamw = New StreamWriter(stream)
            streamr = New StreamReader(stream)

            'RSA Public Key Exchange
            streamw.WriteLine(RSA_key.open_key) : streamw.Flush()
            pub_key = streamr.ReadLine

            'Diffi Hellman
            SendHandshakeString(Cryption.RSA_ver(p.ToString, pub_key))
            SendHandshakeString(Cryption.RSA_ver(g.ToString, pub_key))
            SendHandshakeString(Cryption.RSA_ver(_A.ToString, pub_key))

            Dim tmp As String = streamr.ReadLine
            If CheckCerf(tmp) = True Then
                K = Cryption.MD5StringHash(Modular_Pow(Cryption.RSA_ent(GetHandshakeMessage(tmp), RSA_key.privat_key), a, p))
            Else
                Me.Invoke(Delegate_ColorString, txt_rec, "&0" & "&4Hijacked please leave now!")
            End If


            'Handshake
            SendString(nickname)
            SendString(channel)
            SendString(rnd.Next(100, 999) & "&5" & nickname & " has joined." & rnd.Next(100, 999))

            'Starts listening Thread.
            t.Start()
        End If

        'Changes Statuslabel Text
        lbl_status.Text = "Connected!"
        lbl_key.Text = K
        lbl_nickname.Text = nickname
        lbl_channel.Text = channel

        'Change displayed Tab.
        TabControl1.SelectTab(2)
    End Sub

    ''' <summary>
    ''' Listening Thread, handles !USERLIST, !POKE, !MSG and normal Text Messages.
    ''' Every String here is certified.
    ''' </summary>
    ''' <remarks></remarks>
    Sub Listen()
        CheckForIllegalCrossThreadCalls = False
        While client.Connected
            Try
                'Get's String,
                Dim tmp As String = streamr.ReadLine
                'Check if certified.
                If CheckCerf(tmp) = False Then
                    Me.Invoke(Delegate_ColorString, txt_rec, "&0" & "&4Hijacked please leave now!")
                End If
                'Encrypts and Split String.
                tmp = GetMessage(tmp)
                'Identify's String.
                Select Case True
                    Case tmp.StartsWith("!USERLIST ")
                        'Clear Listbox, Split String, Add new Entrys, remove own Nickname.
                        Me.Invoke(Delegate_ClearItems, lb_online)
                        Dim arr As String() = tmp.Replace("!USERLIST ", "").Split({Chr(59)}, StringSplitOptions.RemoveEmptyEntries)
                        Me.Invoke(Delegate_AddRange, arr, lb_online)
                        Me.Invoke(Delegate_Remove, nickname, lb_online)
                    Case tmp.StartsWith("!POKE ")
                        'Handles Poke Event.
                        tmp = tmp.Replace("!POKE ", "")
                        Dim m As Match = Regex.Match(tmp, "(.*) (.*) (.*)")
                        MessageBox.Show(m.Groups(2).ToString.Replace("_", " "), m.Groups(3).ToString & " has poked you!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Case tmp.StartsWith("!FILE ")
                        'Handles File Event.
                        Dim m As Match = Regex.Match(tmp, "!FILE (.*) (.*) (.*)")
                        If MessageBox.Show(m.Groups(1).ToString & " has sent you a File(" & m.Groups(3).ToString & "), would you like to save it?", "File received!", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                            Me.Invoke(Delegate_SaveFile, m.Groups(2).ToString, m.Groups(3).ToString)
                        End If
                    Case Else
                        'Handles 'normal' String receive. Removes first and last 3 random Numbers.
                        tmp = tmp.Remove(0, 3) : tmp = tmp.Substring(0, tmp.Length - 3)
                        'Check if TopMost. -> Notification
                        If Me.TopMost = False Then
                            Console.Beep()
                            If tmp.StartsWith("&5Private Message from: ") Then
                                Dim Pat As String = "&5Private Message from: (.*) -> (.*)"
                                Dim m As Match = Regex.Match(tmp, Pat)
                                Dim str1 As String = m.Groups(1).ToString
                                Dim str2 As String = m.Groups(2).ToString
                                NotifyIcon1.BalloonTipTitle = str1 & " sent you a Message!"
                                NotifyIcon1.BalloonTipText = str2
                            Else
                                NotifyIcon1.BalloonTipTitle = "New Message!"
                                NotifyIcon1.BalloonTipText = tmp
                            End If
                            NotifyIcon1.ShowBalloonTip(5000)
                        End If
                        'Write received String into RichTextBox.
                        Me.Invoke(Delegate_Mark, txt_rec, "[" & My.Computer.Clock.LocalTime.Hour & ":" & My.Computer.Clock.LocalTime.Minute & "] ", Color.DodgerBlue)
                        Me.Invoke(Delegate_ColorString, txt_rec, "&0" & tmp)
                        Me.Invoke(Delegate_AppendText, vbNewLine, txt_rec)
                End Select
                'Catches every Error and handles it as disconnect.
            Catch ex As Exception
                Console.WriteLine(ex.ToString)
                Me.Invoke(Delegate_AppendText, "Connection aborted!" & vbNewLine, txt_rec)
            End Try
        End While
    End Sub

    ''' <summary>
    ''' Download 'Server List'
    ''' </summary>
    ''' <remarks></remarks>
    Sub Refresh()
        Dim wc As New System.Net.WebClient
        Dim arr As String() = wc.DownloadString("http://pastebin.com/raw.php?i=mprSvMUC").Split({Chr(62)}, StringSplitOptions.RemoveEmptyEntries)
        For Each s As String In arr
            Dim s_ As String() = s.Split({Chr(60)}, StringSplitOptions.RemoveEmptyEntries)
            Me.Invoke(Delegate_AddItem, s_(0), s_(1), lw_server)
        Next
    End Sub

    ''' <summary>
    ''' Handles Enter in txt_in
    ''' Sends the Message.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txt_in_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_in.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            'Checks if empty.
            If Not txt_in.Text.Replace(" ", "") = "" Then
                'Writes String to RichTextBox and sends it.
                Me.Invoke(Delegate_Mark, txt_rec, "[" & My.Computer.Clock.LocalTime.Hour & ":" & My.Computer.Clock.LocalTime.Minute & "] ", Color.Violet) : txt_rec.AppendText("Me: ") : Me.Invoke(Delegate_ColorString, txt_rec, "&0" & txt_in.Text & vbNewLine)
                SendString(rnd.Next(100, 999) & nickname & ": " & txt_in.Text & rnd.Next(100, 999))
                txt_in.Clear()
            Else
                MessageBox.Show("Please enter a Text!", "Error!")
                txt_in.Clear()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Handles Change Event of Prime Checkbox.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cb_adop_CheckedChanged(sender As Object, e As EventArgs) Handles cb_adop.CheckedChanged
        If cb_adop.Checked = True Then
            ud_prime.Enabled = True
        Else
            ud_prime.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Handles DoubleClick on Serverchooser.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lw_server_DoubleClick(sender As Object, e As EventArgs) Handles lw_server.DoubleClick
        For Each lw As ListViewItem In lw_server.Items
            If lw.Index = lw_server.SelectedIndices(0) Then
                txt_ip.Text = lw.Text
            End If
        Next
        txt_ip.ReadOnly = True
        TabControl1.SelectTab(1)
    End Sub

    ''' <summary>
    ''' Handles Button Own Server Click.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_ownsvr_Click(sender As Object, e As EventArgs) Handles btn_ownsvr.Click
        TabControl1.SelectTab(1)
    End Sub

    ''' <summary>
    ''' Handles ToolStripMenuItem KeyInformations Click, show's Debug Form.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub KeyInformationsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeyInformationsToolStripMenuItem.Click
        Dim frm As New frmKey(RSA_key.privat_key, RSA_key.open_key, K)
        frm.Show()
    End Sub

    ''' <summary>
    ''' Handles btn_refresh.click on StartForm, for refreshing Master Server List.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_refresh_Click(sender As Object, e As EventArgs) Handles btn_refresh.Click
        r.Start()
    End Sub

#Region "ToolStripMenu, PM/Poke"
    ''' <summary>
    ''' Handles Private Message ContextmenuStrip Button.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub SendPrivateMessageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SendPrivateMessageToolStripMenuItem.Click
        Dim rec As String = lb_online.SelectedItem
        Dim msg As String = InputBox("Please enter a Private Message..")
        SendString("!MSG " & rec & " " & msg)
        Me.Invoke(Delegate_ColorString, txt_rec, "&4Private Message to : " & rec & " -> " & msg.Replace("_", " ") & vbNewLine)
    End Sub

    ''' <summary>
    ''' Handles Poke ContextmenuStrip Button.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub PokeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PokeToolStripMenuItem.Click
        Dim rec As String = lb_online.SelectedItem
        Dim msg As String = InputBox("Please enter a Poke Message..").Replace(" ", "_")
        SendString("!POKE " & rec & " " & msg & " " & nickname)
    End Sub

    ''' <summary>
    ''' Handles Send File ContextmenuStrip Button.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub SendFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SendFileToolStripMenuItem.Click
        Dim rec As String = lb_online.SelectedItem
        Dim ofd As New OpenFileDialog
        ofd.ShowDialog()
        SendString("!FILE " & rec & " " & Convert.ToBase64String(System.IO.File.ReadAllBytes(ofd.FileName)) & " " & ofd.SafeFileName)
    End Sub
#End Region

#Region "Function/Subs"
    ''' <summary>
    ''' Modular Pow needed for Diffi Hellman.
    ''' </summary>
    ''' <param name="a"></param>
    ''' <param name="b"></param>
    ''' <param name="m"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Modular_Pow(ByVal a As Long, ByVal b As Long, ByVal m As Long) As Long
        Dim result As Long = 1
        While b > 0
            If (b And 1) = 1 Then
                result = (result * a) Mod m
            End If
            b >>= 1
            a = (a * a) Mod m
        End While
        Return result
    End Function

    ''' <summary>
    ''' Sends String.
    ''' Encrypts it -> Certificates it.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <remarks></remarks>
    Private Sub SendString(ByVal msg As String)
        msg = Cryption.Rijndaelcrypt(msg, K)
        streamw.WriteLine(msg & " ~ " & Cryption.RSA_ver(Cryption.MD5StringHash(msg), pub_key)) : streamw.Flush()
    End Sub

    ''' <summary>
    ''' Sends String.
    ''' Certificates it.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <remarks></remarks>
    Private Sub SendHandshakeString(ByVal msg As String)
        streamw.WriteLine(msg & " ~ " & Cryption.RSA_ver(Cryption.MD5StringHash(msg), pub_key)) : streamw.Flush()
    End Sub

    ''' <summary>
    ''' Check String, if it's certificated.
    ''' </summary>
    ''' <param name="msg">Received Message</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckCerf(ByVal msg As String) As Boolean
        Dim arry As String() = msg.Split(" ~ ")
        Dim tmp_hash As String = Cryption.RSA_ent(arry(2), RSA_key.privat_key)
        If Not Cryption.MD5StringHash(arry(0)) = tmp_hash Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Extracts the Message without Certificat.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetMessage(ByVal msg As String) As String
        Dim arry As String() = msg.Split(" ~ ")
        Return Cryption.RijndaelDecrypt(arry(0), K)
    End Function

    ''' <summary>
    ''' Extracts the Message without Certificat.
    ''' </summary>
    ''' <param name="msg">Receives Message</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetHandshakeMessage(ByVal msg As String) As String
        Dim arry As String() = msg.Split(" ~ ")
        Return arry(0)
    End Function
#End Region

#Region "Exit/Close"
    ''' <summary>
    ''' Tries to send Server "!EXIT" command.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            SendString("!EXIT")
        Catch : End Try
    End Sub

    ''' <summary>
    ''' Closes Application ;)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub
#End Region
End Class
